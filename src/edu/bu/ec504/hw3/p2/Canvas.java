package edu.bu.ec504.hw3.p2;

import edu.bu.ec504.hw3.p2.Canvas.CanvasPoint;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 */
public abstract class Canvas implements Iterable<CanvasPoint> {
    /**
     * Initialized the canvas for a set of points on the
     * <code>theXsize</code> x <code>theYsize</code> plane.
     * @param theXsize The X dimension of the support plain for points.
     * @param theYsize The Y dimension of the support plain for points.
     */
    public Canvas(double theXsize, double theYsize) {
        Xsize=theXsize; Ysize = theYsize;
        points = new ArrayList<>();
    }

    /**
     * Adds a new point to the data structure at location (xx,yy).
     * @param pnt The point to add.  It must be that:
     *   The X coordinate of the point: 0<= pnt.x <={{@link #Xsize}}.
     *   The Y coordinate of the point. 0<=pnt.y <={{@link #Ysize}}.
     * @throws IllegalArgumentException if the point is out of the bounds of this Canvas.
     * @note: There can be only one point at any given location.
     * @note: You may want to override this method.
     */
    public void addPoint(CanvasPoint pnt) throws IllegalArgumentException {
        if (_checkPoint(pnt))
            points.add(pnt);
        else
            throw new IllegalArgumentException("Point coordinates off canvas:" + pnt);
    }

    /**
     * Deletes the point at location <code>pnt</code> in the plane, if it exists.
     * @param pnt The point to delete.  It must be that:
     *   The X coordinate of the point: 0<= pnt.x <={{@link #Xsize}}.
     *   The Y coordinate of the point. 0<= pnt.y <={{@link #Ysize}}.
     * @return true iff the deletion was successful (e.g., there was a point at the given location)
     * @note: You may want to override this method.
     */
    public boolean deletePoint(CanvasPoint pnt) {
        if (_checkPoint(pnt))
            return points.remove(pnt);
        else
            return false;
    }

    /**
     * Produce a covering of the entire canvas.  More specifically,
     * produce a list of centers such that each point on the canvas
     * is within a unit-radius circle around at least one of the centers.
     *
     * @return A list of centers of unit circles.
     */
    public abstract ArrayList<CanvasPoint> generateCovering();

    // HELPERS

    /**
     * @return An iterator of the points on this Canvas.
     */
    @Override
    public Iterator<CanvasPoint> iterator() {
        return points.iterator();
    }

    /**
     * @param centers The centers of circles of a covering.
     * @return true iff unit circles centered at the points in <code>centers</code>
     *    cover all the points in this data structure.
     */
    final public boolean checkCovering(ArrayList<CanvasPoint> centers) {
        // check each point, one by one
        for (CanvasPoint pnt: this) {
            boolean foundCenter = false;
            // look at each center to see if one of them is of distance <=1 from the point
            for (CanvasPoint center: centers) {
                if (_distance(center,pnt) <= 1.0f) {
                    foundCenter = true;
                    break;
                }
            }
            if (!foundCenter)
                return false;
        }
        return true;
    }

    /**
     * @param pnt The point being checked.
     * @return true iff <code>pnt</code> is within the bounds of this Canvas
     */
    private boolean _checkPoint(CanvasPoint pnt) {
        if ((pnt.x<0) || (pnt.x>Xsize) || (pnt.y<0) || (pnt.y>Ysize))
            return false;
        else
            return true;
    }

    private double _distance(CanvasPoint pnt1, CanvasPoint pnt2) {
        return Math.sqrt(
            Math.pow(pnt1.x - pnt2.x,2) +
            Math.pow(pnt1.y - pnt2.y,2)
        );
    }

    // NESTED SUBCLASSES

    /**
     * A point on the canvas.
     */
    final public static class CanvasPoint {
        CanvasPoint(double theX, double theY) {
            x = theX; y=theY;
        }

        @Override
        public String toString() {
            return "CanvasPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
        }

        final public double x;
        final public double y;
    }

    // FIELDS
    protected final double Xsize;
    protected final double Ysize;
    protected final ArrayList<CanvasPoint> points;

}
