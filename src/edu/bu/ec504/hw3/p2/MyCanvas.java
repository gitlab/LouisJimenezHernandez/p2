package edu.bu.ec504.hw3.p2;

import java.util.ArrayList;

public class MyCanvas extends Canvas {

  /**
   * Initialized the canvas for a set of points on the
   * <code>theXsize</code> x <code>theYsize</code> plane.
   *
   * @param theXsize The X dimension of the support plain for points.
   * @param theYsize The Y dimension of the support plain for points.
   */
  public MyCanvas(int theXsize, int theYsize) {
    super(theXsize, theYsize);
  }

  @Override
  public ArrayList<CanvasPoint> generateCovering() {
    // This demo implementation generates a trivial covering, where there is
    // a circle around each point.
    ArrayList<CanvasPoint> centers = new ArrayList<>();
    for (CanvasPoint pnt: this) {
      centers.add(new CanvasPoint(pnt.x, pnt.y));
    }
    return centers;
  }
}
